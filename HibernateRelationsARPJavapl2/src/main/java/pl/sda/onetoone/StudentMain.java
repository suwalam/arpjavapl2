package pl.sda.onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.time.LocalDate;

public class StudentMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Student student = new Student(null, "Jan", "Kowalski", LocalDate.of(1999, 6, 9), null);
        StudentIndex studentIndex = new StudentIndex(null, "12345", student);
        student.setStudentIndex(studentIndex);

        session.save(student);
        session.save(studentIndex);

        StudentIndex studentIndexFromDB = session.get(StudentIndex.class, 1);

        System.out.println("Surname for Student with id=1: " + studentIndexFromDB.getStudent().getSurname());

        Student studentFromDB = session.get(Student.class, 1);
        System.out.println("Index number for Student with id=1: " + studentFromDB.getStudentIndex().getNumber());

        System.out.println(studentFromDB);

        String studentSurname = "Kowalski";
        Query query = session.createQuery("FROM Student s WHERE s.surname = :p1", Student.class);
        query.setParameter("p1", studentSurname);
        query.getResultList().forEach(System.out::println);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }

}
