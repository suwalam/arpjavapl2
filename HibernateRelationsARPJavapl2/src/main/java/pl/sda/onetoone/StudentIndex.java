package pl.sda.onetoone;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class StudentIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String number;

    @JoinColumn(name = "student_fk")
    @OneToOne
    private Student student;

    @Override
    public String toString() {
        return "StudentIndex{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", student=" + student +
                '}';
    }
}
