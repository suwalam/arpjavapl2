package pl.sda.arguments;

public class PersonMain {

    public static void main(String[] args) {

        Person person = new Person("Jan", "Kowalski");
        setPersonName(person, "Michał");
        System.out.println(person);

        int v = 5;
        setIntValue(v);
        System.out.println(v);


    }

    //Przekazywanie argumentu przez referencję
    private static void setPersonName(Person person, String toName) {
        person.setName(toName);
    }

    //Przekazywanie argumentu przez wartość
    private static void setIntValue(int value) {
        value = 10;
        System.out.println(value);
    }

}
