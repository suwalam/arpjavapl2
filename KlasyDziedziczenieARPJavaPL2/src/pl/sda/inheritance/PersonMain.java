package pl.sda.inheritance;

public class PersonMain {

    public static void main(String[] args) {

//        Person person = new Person("Jan", "Kowalski", "12345678910");
//        person.printYourself(); //nie można utworzyć obiektu klasy abstrakcyjnej

        Person student = new Student("Anna", "Nowak", "12345678911", "IT");
        student.printYourself();

    }

}
