package pl.sda.nested;

public class AnonymousMain {

    public static void main(String[] args) {

        AnonymousAbstractClass obj = new AnonymousAbstractClass() {

            @Override
            public void print() {
                System.out.println("print from anonymous");
            }
        };

        obj.print();
    }

}
