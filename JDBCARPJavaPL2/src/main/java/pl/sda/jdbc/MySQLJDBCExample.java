package pl.sda.jdbc;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.*;

public class MySQLJDBCExample {

    public static void main(String[] args) {

        String mysqlUrl = "jdbc:mysql://localhost:3306/jdbcexamplearpjavapl2?serverTimezone=Europe/Warsaw";
        String mysqlUser = "root";
        String mysqlPassword = "root";

        try {

            //Alternatywny sposób na pozyskiwanie obiektu typu Connection
            /*MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setUrl(mysqlUrl);
            dataSource.setUser(mysqlUser);
            dataSource.setPassword(mysqlPassword);

            Connection connectionFromDatasource = dataSource.getConnection(); */

            Connection connection = DriverManager.getConnection(mysqlUrl, mysqlUser, mysqlPassword);





            Statement statement = connection.createStatement();

            String nameToUpdate = "Maria";//;SELECT * FROM USERS;
            int whereId = 2;

            final PreparedStatement preparedStatement = connection.prepareStatement("UPDATE person SET FIRST_NAME = ? WHERE ID = ?");
            preparedStatement.setString(1, nameToUpdate);
            preparedStatement.setInt(2, whereId);

            int updatedRows = preparedStatement.executeUpdate();
            System.out.println("Ilosc zaktualizowanych wierszy: " + updatedRows);

            //Nie powinno się sklejać zapytań SQL za pomocą Stringów
            //statement.executeUpdate("UPDATE person SET FIRST_NAME = " + nameToUpdate  + " WHERE ID = " + whereId);

            ResultSet resultSet = statement.executeQuery("SELECT * FROM person");

            final CallableStatement callableStatement = connection.prepareCall("{call select_all_persons()}");
            resultSet = callableStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                String firstName = resultSet.getString("FIRST_NAME");
                String lastName = resultSet.getString("LAST_NAME");
                String pesel = resultSet.getString("PESEL");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }



            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
