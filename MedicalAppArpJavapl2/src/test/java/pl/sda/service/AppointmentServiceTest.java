package pl.sda.service;

import org.junit.jupiter.api.Test;
import pl.sda.entity.Appointment;
import pl.sda.entity.Doctor;
import pl.sda.entity.Patient;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class AppointmentServiceTest extends AbstractServiceTest {

    @Test
    public void shouldBookAppointmentForPatientAndDoctor() {
        //given
        PatientService patientService = new PatientService();
        DoctorService doctorService = new DoctorService();
        AppointmentService underTest = new AppointmentService();
        LocalDateTime dateFrom = LocalDateTime.parse("2022-06-10 11:00", formatter);

        Patient testPatient = new Patient(null, patientName, patientSurname, pesel, null, null);
        Doctor testDoctor = new Doctor(null, doctorName, doctorSurname, doctorIdentifier, null, null);
        patientService.save(testPatient);
        doctorService.save(testDoctor);


        //when
        underTest.book(pesel, doctorIdentifier, dateFrom);

        //then
        Patient patientFromDB = patientService.getByPesel(pesel);
        Appointment appointmentFromPatient = patientFromDB.getAppointments().get(0);
        assertEquals(dateFrom, appointmentFromPatient.getDateFrom());

    }

    @Test
    public void shouldCancelAppointmentForPatient() {
        //given
        PatientService patientService = new PatientService();
        DoctorService doctorService = new DoctorService();
        AppointmentService underTest = new AppointmentService();
        LocalDateTime dateFrom = LocalDateTime.parse("2022-06-10 11:00", formatter);

        Patient testPatient = new Patient(null, patientName, patientSurname, pesel, null, null);
        Doctor testDoctor = new Doctor(null, doctorName, doctorSurname, doctorIdentifier, null, null);
        patientService.save(testPatient);
        doctorService.save(testDoctor);

        underTest.book(pesel, doctorIdentifier, dateFrom);


        //when
        underTest.cancel(pesel, null, dateFrom);

        //then
        Patient patientFromDB = patientService.getByPesel(pesel);
        Appointment appointmentFromPatient = patientFromDB.getAppointments().get(0);
        assertTrue(appointmentFromPatient.isCancelled());

    }


}