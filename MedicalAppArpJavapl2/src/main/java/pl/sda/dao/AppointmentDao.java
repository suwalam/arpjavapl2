package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.entity.Appointment;
import pl.sda.util.HibernateUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class AppointmentDao extends AbstractDao<Appointment> {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public AppointmentDao() {
        super(Appointment.class);
    }

    public Appointment getByPeselAndDateFrom(String pesel, LocalDateTime dateFrom) {
        String hql = "FROM Appointment WHERE patient.pesel = :p1 AND dateFrom = :p2";
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(hql);
        query.setParameter("p1", pesel);
        query.setParameter("p2", LocalDateTime.parse(dateFrom.format(formatter), formatter));

        List<Appointment> resultList = query.getResultList();
        session.close();

        return resultList.stream()
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No appointment for pesel "
                        + pesel + ", dateFrom " + dateFrom.format(formatter)));
    }

    public Appointment getByDoctorIdentifierAndDateFrom(String identifier, LocalDateTime dateFrom) {
        String hql = "FROM Appointment WHERE doctor.identifier = :p1 AND dateFrom = :p2";
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(hql);
        query.setParameter("p1", identifier);
        query.setParameter("p2", LocalDateTime.parse(dateFrom.format(formatter), formatter));

        List<Appointment> resultList = query.getResultList();
        session.close();

        return resultList.stream()
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No appointment for doctor identifier "
                        + identifier + ", dateFrom " + dateFrom.format(formatter)));
    }
}
