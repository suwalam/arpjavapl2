package pl.sda.main;

import pl.sda.entity.Appointment;
import pl.sda.entity.Doctor;
import pl.sda.entity.SpecialityType;
import pl.sda.service.AppointmentService;
import pl.sda.service.DoctorService;
import pl.sda.service.PatientService;
import pl.sda.service.SpecialityService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class MedicalAppMain {

    private static PatientService patientService = new PatientService();

    private static DoctorService doctorService = new DoctorService();

    private static AppointmentService appointmentService = new AppointmentService();

    private static SpecialityService specialityService = new SpecialityService();

    private static Scanner scanner = new Scanner(System.in);

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public static void main(String[] args) {

        int choose;

        do {
            System.out.println("Wybierz opcję: ");
            System.out.println("0 - zakończ");
            System.out.println("1 - Zarejestruj pacjenta");
            System.out.println("2 - Zarejestruj lekarza");
            System.out.println("3 - Zarezerwuj wizytę");
            System.out.println("4 - Wyświetl przyszłe wizyty pacjenta");
            System.out.println("5 - Anuluj wizytę pacjenta");

            choose = scanner.nextInt();

            switch (choose) {
                case 0: System.exit(0);
                case 1: registerPatient(); break;
                case 2: registerDoctorWithSpeciality(); break;
                case 3: bookAppointment(); break;
                case 4: showUpcomingAppointmentsForPatient(); break;
                case 5: cancelAppointmentForPatient(); break;
                default: System.out.println("Nierozpoznana operacja"); break;
            }
        } while (true);
    }

    private static void registerPatient() {
        System.out.println("Podaj imię: ");
        String name = scanner.next();

        System.out.println("Podaj nazwisko: ");
        String surname = scanner.next();

        System.out.println("Podaj PESEL: ");
        String pesel = scanner.next();

        System.out.println("Podaj miasto: ");
        String city = scanner.next();

        System.out.println("Podaj ulicę: ");
        String street = scanner.next();

        System.out.println("Podaj numer: ");
        String number = scanner.next();

        boolean result = patientService.createPatientWithAddress(name, surname, pesel, city, street, number);

        if (result) {
            System.out.println("Pomyślnie utworzono pacjenta o numerze PESEL: " + pesel);
        } else {
            System.out.println("Nie utworzono pacjenta o numerze PESEL: " + pesel);
        }
    }

    private static void registerDoctorWithSpeciality() {
        System.out.println("Podaj imię: ");
        String name = scanner.next();

        System.out.println("Podaj nazwisko: ");
        String surname = scanner.next();

        System.out.println("Podaj identyfikator: ");
        String identifier = scanner.next();

        System.out.println("Podaj specjalność lub wpisz BRAK: ");

        for (SpecialityType specType : SpecialityType.values()) {
            System.out.println(specType.name());
        }

        String specialityName = scanner.next();
        specialityName = specialityName.toUpperCase();


        boolean result = doctorService.createDoctorWithSpeciality(name, surname, identifier, SpecialityType.valueOf(specialityName));

        if (result) {
            System.out.println("Pomyślnie utworzono lekarza o identyfikatorze: " + identifier);
        } else {
            System.out.println("Nie utworzono lekarza o identyfikatorze: " + identifier);
        }
    }

    private static void bookAppointment() {
        System.out.println("Podaj PESEL pacjenta: ");
        String pesel = scanner.next();

        System.out.println("Podaj specjalność: ");
        for (SpecialityType specType : SpecialityType.values()) {
            System.out.println(specType.name());
        }
        String specialityName = scanner.next();
        specialityName = specialityName.toUpperCase();

        List<Doctor> doctors = doctorService.getAllBySpecialityName(SpecialityType.valueOf(specialityName));
        for (Doctor doctor : doctors) {
            System.out.println("Identifier: " + doctor.getIdentifier() + ", Name: "
                    + doctor.getName() + ", Surname: " + doctor.getSurname());
        }

        System.out.println("Podaj identyfikator lekarza: ");
        String identifier = scanner.next();

        //"2022-06-10 11:00"
        System.out.println("Podaj datę wizyty: ");
        String date = scanner.next();

        System.out.println("Podaj godzinę wizyty: ");
        String time = scanner.next();

        appointmentService.book(pesel, identifier, LocalDateTime.parse(date + " " + time, formatter));
    }

    private static void showUpcomingAppointmentsForPatient() {
        System.out.println("Podaj PESEL pacjenta: ");
        String pesel = scanner.next();

        List<Appointment> futureAppointments = patientService.getFutureAppointments(pesel);

        for (Appointment appointment : futureAppointments) {
            System.out.println("Data wizyty: " + appointment.getDateFrom()
                     + " u lekarza: " +  appointment.getDoctor().getName()
                    + " " + appointment.getDoctor().getSurname());
        }
    }

    private static void cancelAppointmentForPatient() {
        System.out.println("Podaj PESEL pacjenta: ");
        String pesel = scanner.next();

        List<Appointment> futureAppointments = patientService.getFutureAppointments(pesel);

        for (Appointment appointment : futureAppointments) {
            System.out.println("Data wizyty: " + appointment.getDateFrom()
                    + " u lekarza: " +  appointment.getDoctor().getName()
                    + " " + appointment.getDoctor().getSurname());
        }

        System.out.println("Którą wizytę anulować? Podaj datę:");

        System.out.println("Podaj datę wizyty: ");
        String date = scanner.next();

        System.out.println("Podaj godzinę wizyty: ");
        String time = scanner.next();

        appointmentService.cancel(pesel, null,  LocalDateTime.parse(date + " " + time, formatter));
    }

}
