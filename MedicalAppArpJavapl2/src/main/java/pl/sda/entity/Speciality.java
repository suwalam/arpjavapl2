package pl.sda.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Speciality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private SpecialityType name;

    @ManyToMany(mappedBy = "specialities", fetch = FetchType.EAGER)
    private List<Doctor> doctors;

    public List<Doctor> getDoctors() {
        return doctors == null ? new ArrayList<>() : doctors;
    }
}
