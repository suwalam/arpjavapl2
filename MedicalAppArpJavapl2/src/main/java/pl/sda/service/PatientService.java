package pl.sda.service;

import pl.sda.dao.AddressDao;
import pl.sda.dao.PatientDao;
import pl.sda.entity.Address;
import pl.sda.entity.Appointment;
import pl.sda.entity.Patient;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class PatientService {

    private PatientDao patientDao = new PatientDao();

    private AddressDao addressDao = new AddressDao();

    public Patient getByPesel(String pesel) {
        return patientDao.getByPesel(pesel);
    }

    public void save(Patient patient) {
        patientDao.save(patient);
    }

    public List<Appointment> getFutureAppointments(String pesel) {
        return getByPesel(pesel)
                .getAppointments()
                .stream()
                .filter(a -> a.isCancelled() == false)
                .filter(a -> a.getDateFrom().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());
    }

    public boolean createPatientWithAddress(String name, String surname, String pesel, String city, String street, String number) {
        if (!patientDao.existsByPesel(pesel)) {
            Address address = new Address(null, city, street, number);
            Patient patient = new Patient(null, name, surname, pesel, address, null);

            addressDao.save(address);
            patientDao.save(patient);
            return true;
        }

        return false;
    }
}
