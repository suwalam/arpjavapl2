package pl.sda.service;

import pl.sda.dao.SpecialityDao;
import pl.sda.entity.Speciality;
import pl.sda.entity.SpecialityType;

public class SpecialityService {

    private SpecialityDao specialityDao = new SpecialityDao();

    public void createSpeciality(SpecialityType type) {
        if (!specialityDao.existsByName(type)) {//jeśli specjalność o typie SpecialityType nie istnieje
            Speciality speciality = new Speciality(null, type, null);
            specialityDao.save(speciality);
        }
    }

}
