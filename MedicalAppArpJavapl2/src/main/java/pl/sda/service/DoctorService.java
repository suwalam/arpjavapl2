package pl.sda.service;

import pl.sda.dao.DoctorDao;
import pl.sda.dao.SpecialityDao;
import pl.sda.entity.Appointment;
import pl.sda.entity.Doctor;
import pl.sda.entity.Speciality;
import pl.sda.entity.SpecialityType;

import javax.print.Doc;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DoctorService {

    private DoctorDao doctorDao = new DoctorDao();

    private SpecialityDao specialityDao = new SpecialityDao();

    public void save(Doctor doctor) {
        doctorDao.save(doctor);
    }

    public Doctor getByIdentifier(String identifier) {
        return doctorDao.getByIdentifier(identifier);
    }

    public List<Appointment> getFutureAppointments(String identifier) {
        return getByIdentifier(identifier)
                .getAppointments()
                .stream()
                .filter(a -> a.isCancelled() == false)
                .filter(a -> a.getDateFrom().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());
    }

    public void addSpeciality(String identifier, SpecialityType type) {
        //pobrać z bazy specjalność o danej nazwie lub ją utworzyć
        Speciality speciality = specialityDao.getByName(type);
        Doctor doctor = getByIdentifier(identifier);

        if (speciality == null) {
            speciality = new Speciality(null, type, Arrays.asList(doctor));
            specialityDao.save(speciality);
        } else {
            speciality.getDoctors().add(doctor);
            specialityDao.update(speciality);
        }

        doctor.getSpecialities().add(speciality);
        doctorDao.update(doctor);
    }

    public List<Doctor> getAllBySpecialityName(SpecialityType type) {
        //zwróć wszystkich lekarzy o danej specjalności
        return specialityDao.getByName(type).getDoctors();
    }

    public List<String> getSpecialities(String identifier) {
        return getByIdentifier(identifier)
                .getSpecialities()
                .stream()
                .map(s -> s.getName().name())
                .collect(Collectors.toList());
    }

    public boolean createDoctorWithSpeciality(String name, String surname, String identifier, SpecialityType... types) {
        if (!doctorDao.existsByIdentifier(identifier)) {
            Doctor doctor = new Doctor(null, name, surname, identifier, null, null);
            doctorDao.save(doctor);

            for (SpecialityType type : types) {
                addSpeciality(identifier, type);
            }
            return true;
        }
        return false;
    }
}
