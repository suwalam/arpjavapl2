package pl.sda.service;

import pl.sda.dao.AppointmentDao;
import pl.sda.dao.DoctorDao;
import pl.sda.dao.PatientDao;
import pl.sda.entity.Appointment;
import pl.sda.entity.Doctor;
import pl.sda.entity.Patient;

import java.time.LocalDateTime;
import java.util.List;

public class AppointmentService {

    public static final int APPOINTMENT_DURATION_MINUTES = 20;

    private PatientDao patientDao = new PatientDao();

    private DoctorDao doctorDao = new DoctorDao();

    private AppointmentDao appointmentDao = new AppointmentDao();

    public void book(String patientPesel, String doctorIdentifier, LocalDateTime dateFrom) {
        LocalDateTime dateTo = dateFrom.plusMinutes(APPOINTMENT_DURATION_MINUTES);
        //pobrać pacjenta po peselu +
        Patient patient = patientDao.getByPesel(patientPesel);

        //pobrać lekarza po identyfikatorze +
        Doctor doctor = doctorDao.getByIdentifier(doctorIdentifier);

        //utworzyć obiekt i zapisać obiekt wizyta
        Appointment appointment = new Appointment(null, dateFrom, dateTo, false, patient, doctor);

        //zaktualizować listę wizyt w obiekcie pacjent i lekarz
        patient.getAppointments().add(appointment);
        doctor.getAppointments().add(appointment);

        appointmentDao.save(appointment);
        patientDao.update(patient);
        doctorDao.update(doctor);
    }

    public void cancel(String pesel, String identifier, LocalDateTime dateFrom) {
        if (pesel != null) {
            Appointment appointment = appointmentDao.getByPeselAndDateFrom(pesel, dateFrom);
             cancel(appointment);
             return;
         }

        if (identifier != null) {
            Appointment appointment = appointmentDao.getByDoctorIdentifierAndDateFrom(identifier, dateFrom);
            cancel(appointment);
            return;
        }
    }

    private void cancel(Appointment appointment) {
        appointment.setCancelled(true);
        appointmentDao.update(appointment);
    }



}
