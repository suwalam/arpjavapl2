package pl.sda.mappedsuperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Beverage extends Product {

    private double capacity;

    public Beverage() {

    }

    public Beverage(Integer id, String name, BigDecimal price, String kind, double capacity) {
        super(id, name, price, kind);
        this.capacity = capacity;
    }
}
