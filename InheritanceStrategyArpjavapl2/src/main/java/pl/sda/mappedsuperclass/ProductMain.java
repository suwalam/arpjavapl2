package pl.sda.mappedsuperclass;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.math.BigDecimal;

public class ProductMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Product cola = new Beverage(null, "cola", BigDecimal.valueOf(8.99), "gas", 1.75);
        Product cola2 = new Beverage(null, "cola", BigDecimal.valueOf(4.99), "gas", 1.00);
        Product apple = new Beverage(null, "apple", BigDecimal.valueOf(6.49), "no gas", 2.00);
        Product water = new Beverage(null, "water", BigDecimal.valueOf(2.09), "no gas", 1.5);
        session.save(cola);
        session.save(cola2);
        session.save(apple);
        session.save(water);

        //pobierz sumę wartości wszystkich produktów (price) z tabeli Beverage
        Query query = session.createQuery("SELECT SUM(price) FROM Beverage");
        System.out.println("Price SUM: " + query.getSingleResult());

        //pobierz średnią pojemność (capacity) wszystkich napojów z tabeli Beverage
        Query query2 = session.createQuery("SELECT AVG(capacity) FROM Beverage");
        System.out.println("Capacity AVG: " + query2.getSingleResult());

        //pobierz średnią cenę napojów pogrupowanych po typie (gas, no gas)
        Query query3 = session.createQuery("SELECT AVG(b.price) FROM Beverage b GROUP BY b.kind");
        System.out.println("Price AVG: " + query3.getResultList());

        transaction.commit();
        session.close();
        sessionFactory.close();

    }

}
