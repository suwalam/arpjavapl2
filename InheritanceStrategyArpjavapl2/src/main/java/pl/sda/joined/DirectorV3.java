package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "director_joined")
@PrimaryKeyJoinColumn(name = "id")
@Entity
public class DirectorV3 extends EmployeeV3 {
    private String department;

    public DirectorV3() {

    }

    public DirectorV3(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }

}
