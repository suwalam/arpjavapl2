package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "office_emp_joined")
@PrimaryKeyJoinColumn(name = "id")
@Entity
public class OfficeEmployeeV3 extends EmployeeV3 {

    private String skills;

    public OfficeEmployeeV3() {

    }

    public OfficeEmployeeV3(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }
}
