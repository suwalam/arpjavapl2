package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table(name = "employee_joined")
public abstract class EmployeeV3 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    protected String name;

    protected String surname;

    public EmployeeV3() {

    }

    public EmployeeV3(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

}
