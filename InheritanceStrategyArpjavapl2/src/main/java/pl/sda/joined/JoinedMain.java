package pl.sda.joined;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class JoinedMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV3 officeEmployee = new OfficeEmployeeV3("Jan", "Kowalski", "Word");
        EmployeeV3 director = new DirectorV3("Michał", "Nowak", "IT");

        session.save(officeEmployee);
        session.save(director);

        OfficeEmployeeV3 officeEmployeeV3 = session.get(OfficeEmployeeV3.class, 1);
        System.out.println(officeEmployeeV3.getName() + " " + officeEmployeeV3.getSurname() + " " + officeEmployeeV3.getSkills());

        transaction.commit();
        session.close();
        sessionFactory.close();

    }

}
