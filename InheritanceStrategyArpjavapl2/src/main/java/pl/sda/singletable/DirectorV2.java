package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@DiscriminatorValue("DIRECTOR")
@Entity
public class DirectorV2 extends EmployeeV2 {

    private String department;

    public DirectorV2() {

    }

    public DirectorV2(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }

}
