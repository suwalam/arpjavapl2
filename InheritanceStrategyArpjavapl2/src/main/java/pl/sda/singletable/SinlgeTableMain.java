package pl.sda.singletable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class SinlgeTableMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV2 officeEmployee = new OfficeEmployeeV2("Jan", "Kowalski", "Java");
        EmployeeV2 director = new DirectorV2("Michał", "Nowak", "IT");

        session.save(officeEmployee);
        session.save(director);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
