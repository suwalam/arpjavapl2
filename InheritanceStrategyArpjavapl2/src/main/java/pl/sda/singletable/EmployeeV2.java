package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "employee_type")
@Entity
@Table(name = "employee_singletable")
public abstract class EmployeeV2 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    protected String name;

    protected String surname;

    @Column(name = "employee_type", insertable = false, updatable = false)
    protected String employeeType;

    public EmployeeV2() {

    }

    public EmployeeV2(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

}
