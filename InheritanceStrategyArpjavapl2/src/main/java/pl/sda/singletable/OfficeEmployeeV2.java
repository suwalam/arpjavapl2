package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Getter
@Setter
@DiscriminatorValue("OFFICE_EMPLOYEE")
@Entity
public class OfficeEmployeeV2 extends EmployeeV2 {

    private String skills;

    public OfficeEmployeeV2() {

    }

    public OfficeEmployeeV2(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }
}
