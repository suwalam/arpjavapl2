package pl.sda.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "office_emp_tableperclass")
public class OfficeEmployee extends Employee {

    private String skills;

    public OfficeEmployee() {

    }

    public OfficeEmployee(String name, String surname, String skills) {
        super(name, surname);
        this.skills = skills;
    }
}
