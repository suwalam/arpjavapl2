package pl.sda.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "director_tableperclass")
public class Director extends Employee {

    private String department;

    public Director() {

    }

    public Director(String name, String surname, String department) {
        super(name, surname);
        this.department = department;
    }
}
