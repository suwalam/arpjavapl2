package pl.sda.collections;

import java.util.ArrayDeque;
import java.util.PriorityQueue;

public class QueueMain {

    public static void main(String[] args) {

        ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();

        arrayDeque.offer(12);
        arrayDeque.offer(34);
        arrayDeque.offer(32);
        arrayDeque.offerFirst(44);
        arrayDeque.offerFirst(76);

        System.out.println(arrayDeque.element());
        System.out.println(arrayDeque.getLast());

        for (Integer i : arrayDeque) {
            System.out.print(i + " ");
        }

        System.out.println(); //znak nowej linii

        PriorityQueue<String> priorityQueue = new PriorityQueue<>();
        priorityQueue.add("Jan");
        priorityQueue.add("Michal");
        priorityQueue.add("Anna");
        priorityQueue.add("Sylwia");
        priorityQueue.add("Krzysztof");

        //sprawdzić dlaczego nie zachowuje priorytetu
        for (String s : priorityQueue) {
            System.out.print(s + " ");
        }

        System.out.println(); //znak nowej linii
        System.out.println(priorityQueue.peek()); //sprawdza poczatek kolejki
        System.out.println(priorityQueue.poll()); //sprawdza poczatek kolejki i usuwa
        System.out.println(priorityQueue.peek()); //sprawdza poczatek kolejki
        System.out.println(priorityQueue.poll()); //sprawdza poczatek kolejki i usuwa
        System.out.println(priorityQueue.poll()); //sprawdza poczatek kolejki i usuwa
        System.out.println(priorityQueue.poll()); //sprawdza poczatek kolejki i usuwa
        System.out.println(priorityQueue.poll()); //sprawdza poczatek kolejki i usuwa
    }

}
