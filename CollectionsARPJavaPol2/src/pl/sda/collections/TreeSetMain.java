package pl.sda.collections;

import pl.sda.compare.Book;
import pl.sda.compare.BookComparator;

import java.time.LocalDate;
import java.util.SortedSet;
import java.util.TreeSet;

public class TreeSetMain {

    public static void main(String[] args) {

        TreeSet<Integer> treeSet = new TreeSet<>();

        treeSet.add(20);
        treeSet.add(54);
        treeSet.add(70);
        treeSet.add(85);
        treeSet.add(48);
        treeSet.add(38);

        for (Integer i : treeSet) {
            System.out.print(i + " ");
        }

        //+++++++++++++++++++++++++++++++++++++++++++
        System.out.println(); //znak nowej linii

        SortedSet<Integer> tailSet = treeSet.tailSet(48); //elementy włącznie z 48 i większe od 48
        //tailSet.add(10); //rzuca wyjątek IllegalArgumentException jeśli wartość jest mniejsza niż 48

        for (Integer i : tailSet) {
            System.out.print(i + " ");
        }

        //+++++++++++++++++++++++++++++++++++++++++++
        System.out.println(); //znak nowej linii

        SortedSet<Integer> headSet = treeSet.headSet(38);
        headSet.add(5);

        for (Integer i : headSet) {
            System.out.print(i + " ");
        }
        System.out.println(); //znak nowej linii


        for (Integer i : treeSet) {
            System.out.print(i + " ");
        }

        //+++++++++++++++++++++++++++++++++++++++++++
        System.out.println(); //znak nowej linii

        SortedSet<Integer> subSet = treeSet.subSet(48, 70);
        for (Integer i : subSet) {
            System.out.print(i + " ");
        }

        //+++++++++++++++++++++++++++++++++++++++++++
        System.out.println(); //znak nowej linii

        TreeSet<Book> books = new TreeSet<>();
        Book b1 = new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 12345, LocalDate.now().minusYears(100));
        Book b2 = new Book("Pan Tadeusz", "Adam Mickiewicz", 54321, LocalDate.now().minusYears(120));
        Book b3 = new Book("Balladyna", "Juliusz Słowacki", 54311, LocalDate.now().minusYears(110));
        books.add(b1);
        books.add(b2);
        books.add(b3);

        for (Book b : books.descendingSet()) { //DESC
            System.out.println(b);
        }

    }

}
