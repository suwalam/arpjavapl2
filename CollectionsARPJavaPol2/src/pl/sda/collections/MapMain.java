package pl.sda.collections;

import java.util.HashMap;
import java.util.Map;

public class MapMain {

    public static HashMap<Integer, String> map = new HashMap<>();

    public static void main(String[] args) {



        map.put(1, "styczeń");
        map.put(2, "luty");
        map.put(3, "marzec");
        map.put(4, "kwiecień");
        map.put(5, "maj");

        System.out.println(map.get(3)); //zwróć string z pary o kluczu 3

        System.out.println(map.containsKey(6));

        for (Integer i :  map.keySet()) {
            System.out.print(i + " ");
        }

        System.out.println();//znak nowej linii

        for (String v : map.values()) {
            System.out.print(v + " ");
        }

        System.out.println();//znak nowej linii

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println("key: " + entry.getKey() + ", value: " + entry.getValue());
        }

    }

}
