package pl.sda.functional;

import org.apache.commons.lang3.StringUtils;
import pl.sda.compare.Book;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.*;

public class FunctionalMain {

    public static void main(String[] args) {

        Function<String, Integer> aQuantity = (str) -> {
            int sum = 0;
            for (int i =0; i< str.length(); i++) {
                if (str.charAt(i) == 'a') {
                    sum++;
                }
            }
            return sum;
        };

        Integer result = aQuantity.apply("afasffadf");
        System.out.println(result);

        Function<Book, String> titleExtractor = (book) -> book.getTitle();
        String title = titleExtractor.apply(new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 12345, LocalDate.now().minusYears(100)));
        System.out.println(title);

        //***************************************************

        Predicate<String> peselValidator = (str) ->
                str.length() == 11 && StringUtils.isNumeric(str);

        System.out.println("Is PESEL valid? " + peselValidator.test("sgsgsrtrwt")); //false
        System.out.println("Is PESEL valid? " + peselValidator.test("12345678910")); //true
        System.out.println("Is PESEL valid? " + peselValidator.test("1234sfgsfdg")); //false

        //***************************************************

        Consumer<Book> isbnSetter = (book) -> book.setIsbn(987654);
        Book book = new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 12345, LocalDate.now().minusYears(100));

        isbnSetter.accept(book);
        System.out.println("ISBN after set: " + book.getIsbn());

        //***************************************************
        Supplier<String> stringSupplier = () -> "supplier string";
        System.out.println(stringSupplier.get());
        System.out.println(stringSupplier.get());
        System.out.println(stringSupplier.get());

        //***************************************************
        BiFunction<String, Integer, List<String>> stringMultiplier = (str, number) -> {
            List<String> strings = new ArrayList<>();

            for (int i=0; i<number; i++) {
                strings.add(str);
            }

            return strings;
        };

        List<String> stringList = stringMultiplier.apply("str", 10);
        stringList.forEach((sArg) -> System.out.println(sArg)); //wypisz wszystkie elementy z kolekcji

        //***************************************************
        BinaryOperator<Integer> sumOperator = (a, b) -> a + b;
        System.out.println(sumOperator.apply(10, 23));

        //***************************************************
        List<Integer> integers = new ArrayList<>();
        integers.add(23);
        integers.add(54);
        integers.add(12);
        integers.add(16);
        integers.add(548);

        Collections.sort(integers, (a, b) -> b - a ); //porównywanie malejące
        integers.forEach((arg) -> System.out.print(arg + " "));

    }

}
