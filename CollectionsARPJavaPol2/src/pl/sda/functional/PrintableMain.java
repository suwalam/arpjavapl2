package pl.sda.functional;

public class PrintableMain {

    public static void main(String[] args) {

        Printable p = (o) -> System.out.println("Printable result: " + o);
        p.print("string");

        Printable p3 = (o) -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(o);
            }
        };

        p3.print(1000000);


        Printable p2 = new Printable() {
            @Override
            public void print(Object obj) {
                System.out.println("Anonymous result: " + obj);
            }
        };

        p2.print("object");
    }

}
