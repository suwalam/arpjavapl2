package pl.sda.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PersonMain {

    public static void main(String[] args) {

        Person p1 = new Person("Jan", "Kowalski", "89041278911", "Warszawa");
        Person p2 = new Person("Jan", "Matejko", "67050278911", "Kraków");
        Person p3 = new Person("Michał", "Nowak", "79051078911", "Warszawa");
        Person p4 = new Person("Anna", "Nowak", "74101678911", "Poznań");
        Person p5 = new Person("Krzysztof", "Kozak", "76011578911", "Kraków");

        List<Person> personList = Arrays.asList(p1, p2, p3, p4, p5);

//        getPersonsByName(personList, "Marek").forEach(System.out::println);
//        String surname = getPersonSurnameByPesel(personList, "89041278911344563465");
//        System.out.println(surname);

        printAddressByPesel(personList, "76011578911");


    }

    private static List<Person> getPersonsByName(List<Person> personList, String name) {
        List<Person> result = personList.stream()
                .filter(p -> p.getName().equals(name))
                .collect(Collectors.toList());

        if (result.size() == 0) {
            throw new IllegalArgumentException("Brak osoby o podanym imieniu " + name);
        }

        return result;
    }

    private static String getPersonSurnameByPesel(List<Person> personList, String pesel) {
        return personList.stream()
                .filter(p -> p.getPesel().equals(pesel))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Osoba o podanym numerze PESEL nie istnieje " + pesel))
                .getSurname();
    }

    private static void printAddressByPesel(List<Person> personList, String pesel) {
        personList.stream()
                .filter(p -> p.getPesel().equals(pesel))
                .findFirst()
                .ifPresent(p -> System.out.println("Adres osoby o numerze PESEL " + pesel + " to: " + p.getAddress()));
    }

}
