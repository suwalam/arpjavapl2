package pl.sda.stream;

import pl.sda.compare.Book;
import pl.sda.compare.Library;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class BookStreamMain {

    public static void main(String[] args) {

        Book b1 = new Book("Ogniem i Mieczem", "Henryk Sienkiewicz", 12345, LocalDate.now().minusYears(100));
        Book b2 = new Book("Pan Tadeusz", "Adam Mickiewicz", 54321, LocalDate.now().minusYears(120));
        Book b3 = new Book("Balladyna", "Juliusz Słowacki", 54311, LocalDate.now().minusYears(130));
        Book b4 = new Book("W Pustyni i w Puszczy", "Henryk Sienkiewicz", 433222, LocalDate.now().minusYears(140));
        Book b5 = new Book("Dziady", "Adam Mickiewicz", 5434321, LocalDate.now().minusYears(114));
        Book b6 = new Book("Lokomotywa", "Jan Brzechwa", 54341111, LocalDate.now().minusYears(143));

        List<Book> bookList = Arrays.asList(b1, b2, b3, b4, b5, b6);

        //printBooksForGivenAuthor(bookList, "Henryk Sienkiewicz");

        Library lib1 = new Library("Warszawa", Arrays.asList(b1, b2));
        Library lib2 = new Library("Kraków", Arrays.asList(b3, b4));
        Library lib3 = new Library("Poznań", Arrays.asList(b5, b6));
        List<Library> libraries = Arrays.asList(lib1, lib2, lib3);
//        printBooksFromAllLibraries(libraries);

//        boolean isBookInLib = isBookInLibrary(libraries, "Kraków", "Balladyna");
//        System.out.println(isBookInLib);

        //optionalUsage(bookList);

        //printBooksForReleaseDateRange(bookList, LocalDate.now().minusYears(150), LocalDate.now().minusYears(120));

        //printBookReleasedAfterGivenYear(bookList, 1900);

        LocalDate.of(1920, 5, 20); //1920-05-20

        printAllBooksSortedByReleaseDate(libraries);

    }

    private static void printBooksForGivenAuthor(List<Book> books, String author) {
        books.stream()
                .filter((b) -> b.getAuthor().equals(author))
                .map(Book::getTitle)
                .forEach(System.out::println);
    }

    private static void printBooksFromAllLibraries(List<Library> libraries) {
        libraries.stream()
                .map(lib -> lib.getBooks())
                .flatMap(list -> list.stream())
                .forEach(System.out::println);
    }

    //czy w bibliotece o podanej nazwie miasta istnieje książka o podanym tytule
    private static boolean isBookInLibrary(List<Library> libraries, String city, String title) {
        return libraries.stream()
                .filter(lib -> lib.getCity().equals(city))
                .map(lib -> lib.getBooks())
                .flatMap(list -> list.stream())
                .anyMatch(b -> b.getTitle().equals(title));
    }

    private static Optional<Book> getBookByTitle(List<Book> books, String title) {
        return books.stream()
                .filter(b -> b.getTitle().equals(title))
                .findFirst();
    }

    //wypisz tytuł i autora książek, które były wydane w zakresie przekazanych dat
    private static void printBooksForReleaseDateRange(List<Book> books, LocalDate from, LocalDate to) {
        books.stream()
                .filter(b -> b.getReleaseDate().isBefore(to))
                .filter(b -> b.getReleaseDate().isAfter(from))
                .map(b -> b.getTitle() + " - " + b.getAuthor())
                .forEach(System.out::println);
    }

    //wypisz tytuł książek, które zostały wydane po podanym w argumencie roku
    private static void printBookReleasedAfterGivenYear(List<Book> books, int year) {
        books.stream()
                .filter(b -> b.getReleaseDate().getYear() > year)
                .map(b -> b.getTitle())
                .forEach(System.out::println);
    }

    //sortowanie rosnąco
    private static void printAllBooksSortedByReleaseDate(List<Library> libraries) {
        libraries.stream()
                .map(lib -> lib.getBooks())
                .flatMap(list -> list.stream())
                .sorted(Comparator.comparing(Book::getReleaseDate))
                .forEach(System.out::println);
    }

    private static void optionalUsage(List<Book> bookList) {
        Optional<Book> bookOptional = getBookByTitle(bookList, "Balladyna");

        //jeśli książka jest w Optional to ją wypisz
        bookOptional.ifPresent(System.out::println);

        //jeśli książka jest w Optional to ją wypisz - jeśli nie - wypisz domyślną
        System.out.println(bookOptional.orElse(new Book("Domyślna", "Domyślny", 0, LocalDate.now().minusYears(100))));

        //jeśli książka jest w Optional to ją wypisz - jeśli nie - wypisz domyślną
        System.out.println(bookOptional.orElseGet(() -> new Book("Domyślna", "Domyślny", 0, LocalDate.now().minusYears(100))));

        //jeśli książka jest w Optional to ją wypisz - jeśli nie - rzuć wyjątek
        System.out.println(bookOptional.orElseThrow(() -> new IllegalArgumentException("Brak książki")));

        //jeśli książka jest w Optional to ją wypisz
        if (bookOptional.isPresent()) {
            System.out.println(bookOptional.get());
        }
    }
}
