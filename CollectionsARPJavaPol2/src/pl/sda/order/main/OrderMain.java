package pl.sda.order.main;

import pl.sda.order.service.OrderService;

import java.util.Scanner;

public class OrderMain {

    private static OrderService orderService = new OrderService();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int choose = 0;

        do {
            System.out.println("Wybierz opcję: ");
            System.out.println("0 - zakończ");
            System.out.println("1 - utwórz zamówienie");
            System.out.println("2 - dodaj produkt do zamówienia");
            System.out.println("3 - wypisz zamówienie");
            System.out.println("4 - usuń produkt z zamówienia");
            System.out.println("5 - aktualizuj opis produktu z zamówienia");
            System.out.println("6 - wypisz zamówienie posortowane");

            choose = scanner.nextInt();

            switch (choose) {
                case 0: System.exit(0);
                case 1: createOrder(); break;
                case 2: addProductToOrder(); break;
                case 3: printOrder(); break;
                case 4: removeProductFromOrder(); break;
                case 5: updateProductDescription(); break;
                case 6: printSortedOrder(); break;
                default: System.out.println("Nierozpoznana operacja"); break;
        }

        } while (true);
    } //koniec metody main

    private static void createOrder() {
        int orderId = orderService.createOrder();
        System.out.println("Pomyślnie utworzono zamówieine o id " + orderId);
    }

    private static void addProductToOrder() {
        System.out.println("Podaj id zamówienia:");
        int orderId = scanner.nextInt();

        System.out.println("Podaj typ produktu:");
        String type = scanner.next();

        System.out.println("Podaj nazwę produktu:");
        String name = scanner.next();

        System.out.println("Podaj producenta produktu:");
        String producer = scanner.next();

        System.out.println("Podaj opis produktu:");
        String description = scanner.next();

        int productId = orderService.addProductToOrder(orderId, type, name, producer, description);

        System.out.println("Pomyślnie dodano produkt o id " + productId + " do zamówienia o id " + orderId);

    }

    private static void printOrder() {
        System.out.println("Podaj id zamówienia");
        int orderId = scanner.nextInt();

        System.out.println("Zamówienie składa się z produktów:");
        orderService.printOrder(orderId);
    }

    private static void removeProductFromOrder() {
        System.out.println("Podaj id zamówienia:");
        int orderId = scanner.nextInt();

        System.out.println("Podaj id produktu do usunięcia:");
        int productId = scanner.nextInt();

        boolean result = orderService.removeProductFromOrder(orderId, productId);

        if (result) {
            System.out.println("Pomyślnie usunięto produkt o id " + productId + " z zamówienia o id " + orderId);
        } else {
            System.out.println("Nie udało się usunąć produktu o id " + productId + " z zamówienia o id " + orderId);
        }
    }

    private static void updateProductDescription() {
        System.out.println("Podaj id zamówienia:");
        int orderId = scanner.nextInt();

        System.out.println("Podaj id produktu:");
        int productId = scanner.nextInt();

        System.out.println("Podaj nowy opis produktu:");
        String newDescription = scanner.next();

        orderService.updateProductDescription(orderId, productId, newDescription);
    }

    private static void printSortedOrder() {
        System.out.println("Podaj id zamówienia");
        int orderId = scanner.nextInt();

        System.out.println("Zamówienie składa się z posortowanych produktów:");
        orderService.printOrderWithSortedProducts(orderId);
    }

}
