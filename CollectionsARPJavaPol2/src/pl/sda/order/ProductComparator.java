package pl.sda.order;

import pl.sda.order.model.Product;

import java.util.Comparator;

public class ProductComparator implements Comparator<Product> {

    @Override
    public int compare(Product p1, Product p2) {
        return p2.getCreateDate().compareTo(p1.getCreateDate()); //od najmłodszego do najstarszego
    }
}
