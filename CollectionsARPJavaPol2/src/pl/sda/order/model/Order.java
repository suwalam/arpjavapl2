package pl.sda.order.model;

import java.time.LocalDateTime;
import java.util.*;

public class Order {

    private static int counter = 1;

    private Integer id;

    private LocalDateTime createDate;

    private Map<ProductType, List<Product>> productMap;

    public Order() {
        this.id = counter++;
        this.createDate = LocalDateTime.now();
        this.productMap = new HashMap<>();
    }

    public Integer getId() {
        return id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void addProduct(Product product) {
        if (productMap.containsKey(product.getType())) {
            List<Product> products = productMap.get(product.getType());
            products.add(product);
        } else {
            List<Product> products = new ArrayList<>();
            products.add(product);
            productMap.put(product.getType(), products);
        }
    }

    public List<Product> getProductsByType(ProductType productType) {
        return productMap.get(productType);
    }

    public List<Product> getProducts() {
        List<Product> result = new ArrayList<>();

        for(List<Product> products :  productMap.values()) {
            result.addAll(products);
        }

        return result;
    }

    public int size() {
//        int sum = 0;
//
//        for(List<Product> products :  productMap.values()) {
//            sum += products.size();
//        }
//        return sum;

        return productMap.values()
                .stream()
                .map(list -> list.size())
                .reduce((s1, s2) -> s1 + s2)
                .orElse(0);
    }

    public boolean removeProductById(Integer id) {
        Product productToRemove = null;

        for (List<Product> products :  productMap.values()) {
            for (Product product : products) {
                if (product.getId().equals(id)) {
                    productToRemove = product;
                    break;
                }
            }
            if (productToRemove != null) {
                products.remove(productToRemove);
                return true;
            }
        }
        return false;
    }

    public void updateProductDescription(Integer id, String newDescription) {
//        for (List<Product> products :  productMap.values()) {
//            for (Product product : products) {
//                if (product.getId().equals(id)) {
//                    product.setDescription(newDescription);
//                    return;
//                }
//            }
//        }

        productMap.values()
                .stream()
                .flatMap(list -> list.stream())
                .filter(p -> p.getId().equals(id))
                .findFirst()
                .ifPresent(p -> p.setDescription(newDescription));
    }
}
